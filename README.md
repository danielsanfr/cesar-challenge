# CESAR Challenge

Para resolver os desafios propostos eu criei um **projeto Android** em que os desafios de algoritmos foram implementados em um modulo chamado **common**, e para a validação dos mesmos foram usados testes unitários. Enquanto que os desafios que necessitavam de uma aplicação Android, 4 e 6, foram implementados em modulos separados **challengefour** e **challengesix** respectivamente.

## 1.  Replacing characters in place

Implementado na função: `fun Array<Char>.replaceSpaces(trueLength: Int)` 

Complexidade de tempo: **O(n)**

Complexidade de espaço: **O(n)**

## 2. Check words with jumbled letters

Implementado na função: `fun CharSequence.isJumbled(original: String): Boolean`

Complexidade de tempo: **O(n!)**

Complexidade de espaço: **O(n)**

## 3. Check words with typos

Implementado na função: `fun CharSequence.haveTypo(original: String): Boolean`

Complexidade de tempo: **O(n)**

Complexidade de espaço: **O(n)**

## 4. [Android] Search on a list

Foi implementado uma simples **Activity** que lista frutas e o usuário pode pesquisar alguma fruta.

##  5. Remove duplicates on email thread

Implementado na função: `fun <E : Comparable<E>> MyLinkedList<E>.removeDuplicates()`

Complexidade de tempo: **O(n!)**

Complexidade de espaço: **O(n)**

## 6. [Android] Email processor service

Não entendi bem a questão, mas mesmo assim implementei um **BroadcastReceiver** que será chamado quando uma **Intent** com a **action** (_some.email.action.from.another.app_) seja chamado. Esse **BroadcastReceiver** deveria receber os e-mails da forma como veio da outra aplicação e passar apenas uma **LinkedList** para o **Service**.

## 7. Linked List Intersection

Implementado na função: `fun <E : Comparable<E>> MyLinkedList<E>.intersectingNode(other: MyLinkedList<E>): MyLinkedList.Node<E>?`

Assumindo que a implementação da **LinkedList** usada tenha um cache do seu tamanho (a minha implementação foi bem simples e não deveria ser levada em consideração).

Complexidade de tempo: **O(n)**

Complexidade de espaço: **O(n)**