package br.com.danielsan.cesarchallenge.challengesix

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import br.com.danielsan.cesarchallenge.challengesix.models.Email
import br.com.danielsan.cesarchallenge.common.removeDuplicates

/**
 * Created by daniel on 9/7/18
 */
class EmailReceiver : BroadcastReceiver() {

    companion object {
        private const val EXTRA_EMAIL_THREAD = "<package>.email_thread"
    }

    override fun onReceive(context: Context, intent: Intent) {
        val emails = intent.getParcelableArrayExtra(EXTRA_EMAIL_THREAD) as? Array<Email> ?: return

        val emailThread = emails.toMyLinkedList()
        emailThread.removeDuplicates()
        context.startService(EmailService.createIntent(context, emailThread))
    }

}
