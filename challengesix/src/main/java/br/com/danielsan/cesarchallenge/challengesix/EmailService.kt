package br.com.danielsan.cesarchallenge.challengesix

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import br.com.danielsan.cesarchallenge.challengesix.models.Email
import br.com.danielsan.cesarchallenge.common.MyLinkedList

/**
 * Created by daniel on 9/7/18
 */
class EmailService : Service() {

    companion object {
        private const val EXTRA_EMAIL_THREAD = "email_thread"
        fun createIntent(context: Context, emailThread: MyLinkedList<Email>): Intent {
            return Intent(context, EmailService::class.java)
                .putExtra(EXTRA_EMAIL_THREAD, emailThread.toArray())
        }

        private fun MyLinkedList<Email>.toArray(): Array<Email> {
            var node: MyLinkedList.Node<Email>? = null
            return Array(size) {
                node = if (node != null) node!!.next else head
                node!!.item
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val emails = intent?.getParcelableArrayExtra(EXTRA_EMAIL_THREAD) as? Array<Email>
                ?: return super.onStartCommand(intent, flags, startId)

        val emailThread = emails.toMyLinkedList()

        return super.onStartCommand(intent, flags, startId)
    }

}
