package br.com.danielsan.cesarchallenge.challengesix

import br.com.danielsan.cesarchallenge.common.MyLinkedList

/**
 * Created by daniel on 9/8/18
 */

fun <E: Comparable<E>> Array<E>.toMyLinkedList(): MyLinkedList<E> {
    return MyLinkedList<E>().also {
        for (element in this) {
            it.add(element)
        }
    }
}
