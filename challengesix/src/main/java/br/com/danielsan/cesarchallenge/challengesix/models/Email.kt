package br.com.danielsan.cesarchallenge.challengesix.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by daniel on 9/8/18
 */
@Parcelize
data class Email(
    val id: String,
    val to: String,
    val subject: String,
    val body: String
) : Parcelable, Comparable<Email> {

    override fun compareTo(other: Email): Int = id.compareTo(other.id)

}
