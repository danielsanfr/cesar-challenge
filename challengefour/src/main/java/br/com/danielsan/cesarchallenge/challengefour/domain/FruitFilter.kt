package br.com.danielsan.cesarchallenge.challengefour.domain

import android.util.Log
import br.com.danielsan.cesarchallenge.common.haveTypo
import br.com.danielsan.cesarchallenge.common.isJumbled

/**
 * Created by daniel on 9/7/18
 */
class FruitFilter {

    fun isValidFruit(fruit: String): (String) -> Boolean {
        val fruitLowerCase = fruit.toLowerCase()
        return {
            val itLowerCase = it.toLowerCase()
            val result = fruitLowerCase.isBlank()
                    || itLowerCase.startsWith(fruitLowerCase)
                    || fruitLowerCase == itLowerCase
                    || fruitLowerCase.isJumbled(itLowerCase)
                    || fruitLowerCase.haveTypo(itLowerCase)

            if (result) {
                Log.d("FruitFilter", "isValidFruit")
            }

            result
        }
    }

}
