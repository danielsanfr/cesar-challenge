package br.com.danielsan.cesarchallenge.challengefour.presentation.main

/**
 * Created by daniel on 9/7/18
 */
interface MainView {

    fun showFruits(fruitList: List<String>)

}
