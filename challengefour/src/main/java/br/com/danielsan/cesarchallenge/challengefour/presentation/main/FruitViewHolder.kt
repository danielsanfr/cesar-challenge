package br.com.danielsan.cesarchallenge.challengefour.presentation.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

/**
 * Created by daniel on 9/7/18
 */
class FruitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(fruit: String) {
        (itemView as TextView).text = fruit
    }

}
