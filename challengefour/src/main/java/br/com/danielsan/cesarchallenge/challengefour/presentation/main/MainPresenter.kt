package br.com.danielsan.cesarchallenge.challengefour.presentation.main

import android.util.Log
import br.com.danielsan.cesarchallenge.challengefour.data.fruits.FruitsRepository
import br.com.danielsan.cesarchallenge.challengefour.shared.Presenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by daniel on 9/7/18
 */
class MainPresenter(private val fruitsRepository: FruitsRepository) : Presenter<MainView>() {

    override fun start() {
        super.start()
        fruitsRepository.getFruits()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                view?.showFruits(it)
            }, {
                Log.e("MainPresenter", "getFruits", it)
            })
    }

}
