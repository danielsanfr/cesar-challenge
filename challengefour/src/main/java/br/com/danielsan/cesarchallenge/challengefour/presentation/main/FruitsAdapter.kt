package br.com.danielsan.cesarchallenge.challengefour.presentation.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.danielsan.cesarchallenge.challengefour.domain.FruitFilter

/**
 * Created by daniel on 9/7/18
 */
class FruitsAdapter(private val fruitFilter: FruitFilter) : RecyclerView.Adapter<FruitViewHolder>() {

    private var lastFilter =  ""
    private val fruitList = mutableListOf<String>()
    private val fruitFilteredList = mutableListOf<String>()

    override fun getItemCount(): Int = fruitFilteredList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FruitViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return FruitViewHolder(inflater.inflate(android.R.layout.simple_list_item_1, parent, false))
    }

    override fun onBindViewHolder(holder: FruitViewHolder, position: Int) {
        holder.bind(fruitFilteredList[position])
    }

    fun addFruits(fruits: List<String>) {
        fruitList.clear()
        fruitList.addAll(fruits)
        filterFruit(lastFilter)
    }

    fun filterFruit(fruit: String) {
        lastFilter = fruit
        fruitFilteredList.clear()
        fruitList.filterTo(fruitFilteredList, fruitFilter.isValidFruit(fruit))
        notifyDataSetChanged()
    }

}
