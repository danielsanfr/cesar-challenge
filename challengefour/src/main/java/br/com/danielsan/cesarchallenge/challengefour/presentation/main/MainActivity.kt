package br.com.danielsan.cesarchallenge.challengefour.presentation.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import br.com.danielsan.cesarchallenge.challengefour.R
import br.com.danielsan.cesarchallenge.challengefour.data.fruits.FruitsRepositoryImpl
import br.com.danielsan.cesarchallenge.challengefour.databinding.ActivityMainBinding
import br.com.danielsan.cesarchallenge.challengefour.domain.FruitFilter
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by daniel on 9/7/18
 */
class MainActivity : AppCompatActivity(), MainView {

    private val fruitAdapter by lazy { FruitsAdapter(FruitFilter()) }
    private val presenter by lazy { MainPresenter(FruitsRepositoryImpl()) }
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this,
            R.layout.activity_main
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(presenter)

        binding.fruits.adapter = fruitAdapter

        RxTextView.textChanges(binding.search)
            .debounce(300, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                fruitAdapter.filterFruit(it.toString())
            }, {
                Log.e("MainActivity", "textChanges", it)
            })

        presenter.attachView(this)
    }

    override fun showFruits(fruitList: List<String>) {
        fruitAdapter.addFruits(fruitList)
    }

}
