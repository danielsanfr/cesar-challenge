package br.com.danielsan.cesarchallenge.challengefour.shared

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.annotation.CallSuper
import java.lang.ref.WeakReference

/**
 * Created by daniel on 9/7/18
 */
abstract class Presenter<View> : LifecycleObserver {

    private var viewReference: WeakReference<View>? = null
    protected var view: View? = null
        private set
        get() = viewReference?.get()

    @CallSuper
    open fun attachView(viewContract: View) {
        viewReference?.clear()
        viewReference = WeakReference(viewContract)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun start() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun stop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun destroy() {
        detachView()
    }

    @CallSuper
    open fun detachView() {
        viewReference?.clear()
        viewReference = null
    }

}
