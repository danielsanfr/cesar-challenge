package br.com.danielsan.cesarchallenge.challengefour.data.fruits

import io.reactivex.Single

/**
 * Created by daniel on 9/7/18
 */
interface FruitsRepository {

    fun getFruits(): Single<List<String>>

}
