package br.com.danielsan.cesarchallenge.common

import br.com.danielsan.cesarchallenge.common.MyLinkedList.Node
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters

@RunWith(value = Parameterized::class)
open class ChallengeSevenUnitTest(
    private val inputA: MyLinkedList<Int>,
    private val inputB: MyLinkedList<Int>,
    private val output: MyLinkedList.Node<Int>
) {

    companion object {
        @JvmStatic
        @Parameters(name = "{index}: input = {0}, output = {1}")
        fun data(): Collection<Array<Any>> {
            val intersectionNode = MyLinkedList.Node(10, Node(11, Node(12, null)))
            return arrayListOf(
                arrayOf(
                    myLinkedListOf(Node(1, Node(2, intersectionNode))),
                    myLinkedListOf(Node(1, intersectionNode)),
                    intersectionNode
                ),
                arrayOf(
                    myLinkedListOf(Node(1, Node(2, intersectionNode))),
                    myLinkedListOf(Node(1, Node(1, Node(2, Node(1, intersectionNode))))),
                    intersectionNode
                ),
                arrayOf(
                    myLinkedListOf(Node(1, Node(2, Node(3, Node(4, Node(5, intersectionNode)))))),
                    myLinkedListOf(Node(1, Node(2, Node(3, Node(4, Node(5, intersectionNode)))))),
                    intersectionNode
                )
            )
        }
    }

    @Test
    fun `Intersecting node`() {
        assertEquals(output, inputA.intersectingNode(inputB))
    }

}
