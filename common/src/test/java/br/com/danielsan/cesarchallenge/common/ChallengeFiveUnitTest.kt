package br.com.danielsan.cesarchallenge.common

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters

@RunWith(value = Parameterized::class)
open class ChallengeFiveUnitTest(
    private val input: MyLinkedList<Int>,
    private val output: MyLinkedList<Int>
) {

    companion object {
        @JvmStatic
        @Parameters(name = "{index}: input = {0}, output = {1}")
        fun data(): Collection<Array<MyLinkedList<Int>>> {
            return arrayListOf(
                arrayOf(
                    myLinkedListOf(1, 2, 3, 4, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 1, 1, 2, 3, 4, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 1, 2, 3, 3, 4, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 2, 3, 4, 5, 5, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 1, 2, 3, 3, 4, 5, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 2, 2, 3, 4, 4, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 2, 3, 2, 4, 2, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 2, 1, 3, 4, 1, 5),
                    myLinkedListOf(1, 2, 3, 4, 5)
                ),
                arrayOf(
                    myLinkedListOf(1, 2, 2, 3, 4, 4, 5, 1, 2),
                    myLinkedListOf(1, 2, 3, 4, 5)
                )
            )
        }
    }

    @Test
    fun `Remove duplicates`() {
        input.removeDuplicates()
        var nodeI: MyLinkedList.Node<Int>? = input.head
        var nodeO: MyLinkedList.Node<Int>? = output.head
        while (nodeI != null && nodeO != null) {
            assertEquals("$input is different from the $output", nodeO.item, nodeI.item)
            nodeI = nodeI.next
            nodeO = nodeO.next
        }
    }

}
