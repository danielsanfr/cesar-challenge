package br.com.danielsan.cesarchallenge.common

import org.junit.Assert.assertArrayEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters

@RunWith(value = Parameterized::class)
open class ChallengeOneUnitTest(
    private val trueLength: Int,
    private val input: Array<Char>,
    private val output: Array<Char>
) {

    companion object {
        @JvmStatic
        @Parameters(name = "{index}: trueLength = {0}")
        fun data(): Collection<Array<Any>> {
            return arrayListOf(
                arrayOf<Any>(
                    19,
                    arrayOf('U', 's', 'e', 'r', ' ', 'i', 's', ' ', 'n', 'o', 't', ' ', 'a', 'l', 'l', 'o', 'w', 'e', 'd', ' ', ' ', ' ', ' ', ' ', ' '),
                    arrayOf('U', 's', 'e', 'r', '&', '3', '2', 'i', 's', '&', '3', '2', 'n', 'o', 't', '&', '3', '2', 'a', 'l', 'l', 'o', 'w', 'e', 'd')
                ),
                arrayOf<Any>(
                    19,
                    arrayOf('D', 'a', 'n', 'i', 'e', 'l', ' ', 'S', ' ', 'F', ' ', 'd', 'a', ' ', 'R', 'o', 'c', 'h', 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
                    arrayOf('D', 'a', 'n', 'i', 'e', 'l', '&', '3', '2', 'S', '&', '3', '2', 'F', '&', '3', '2', 'd', 'a', '&', '3', '2', 'R', 'o', 'c', 'h', 'a')
                ),
                arrayOf<Any>(
                    2,
                    arrayOf(' ', ' ', ' ', ' ', ' ', ' '),
                    arrayOf('&', '3', '2', '&', '3', '2')
                ),
                arrayOf<Any>(
                    6,
                    arrayOf(' ', ' ', 'T', 'e', 's', 't', ' ', ' ', ' ', ' '),
                    arrayOf('&', '3', '2', '&', '3', '2', 'T', 'e', 's', 't')
                ),
                arrayOf<Any>(
                    12,
                    arrayOf(' ', ' ', 'T', 'e', 's', 't', ' ', ' ', 't', 'i', 'n', 'g', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
                    arrayOf('&', '3', '2', '&', '3', '2', 'T', 'e', 's', 't', '&', '3', '2', '&', '3', '2', 't', 'i', 'n', 'g')
                ),
                arrayOf<Any>(
                    1,
                    arrayOf(' ', ' ', ' '),
                    arrayOf('&', '3', '2')
                )
            )
        }
    }

    @Test
    fun `Replace is correct`() {
        input.replaceSpaces(trueLength)
        assertArrayEquals(output, input)
    }

}
