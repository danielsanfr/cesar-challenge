package br.com.danielsan.cesarchallenge.common

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters

@RunWith(value = Parameterized::class)
open class ChallengeTwoUnitTest(
    private val original: String,
    private val input: String,
    private val expected: Boolean
) {

    companion object {
        @JvmStatic
        @Parameters(name = "{index}: original = {0}, input = {1}, expected = {2}")
        fun data(): Collection<Array<Any>> {
            return arrayListOf(
                arrayOf("you", "yuo", true),
                arrayOf("probably", "porbalby", true),
                arrayOf("despite", "desptie", true),
                arrayOf("moon", "nmoo", false),
                arrayOf("misspellings", "mpeissngslli", false),
                arrayOf("akee", "aple", false),
                arrayOf("daniel", "daniel", true),
                arrayOf("daniel", "denial", true),
                arrayOf("daniel", "delian", true),
                arrayOf("daniel", "doniel", false),
                arrayOf("daniel", "danilo", false)
            )
        }
    }

    @Test
    fun `Is a partial-permutation`() {
        assertEquals(expected, input.isJumbled(original))
    }

}
