package br.com.danielsan.cesarchallenge.common

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters

@RunWith(value = Parameterized::class)
open class ChallengeThreeUnitTest(
    private val original: String,
    private val input: String,
    private val expected: Boolean
) {

    companion object {
        @JvmStatic
        @Parameters(name = "{index}: original = {0}, input = {1}, expected = {2}")
        fun data(): Collection<Array<Any>> {
            return arrayListOf(
                arrayOf("love", "loove", true),
                arrayOf("pale", "ple", true),
                arrayOf("pales", "pale", true),
                arrayOf("pale", "bale", true),
                arrayOf("pale", "bake", false),
                arrayOf("Daniel", "D@ni&l", false),
                arrayOf("Daniel", "Diel", false),
                arrayOf("San", "S", false)
            )
        }
    }

    @Test
    fun `They are one typo (or zero typos) away`() {
        assertEquals(expected, input.haveTypo(original))
    }

}
