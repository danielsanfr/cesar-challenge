package br.com.danielsan.cesarchallenge.common

import kotlin.math.absoluteValue

/**
 * Created by daniel on 9/7/18
 */

fun CharSequence.isJumbled(original: String): Boolean {
    if (length != original.length || get(0) != original[0]) {
        return false
    }

    val chars = Array(length) { ' ' }
    var charsLength = 0
    var changes = 0
    val maxOfChanges = length * 2 / 3
    for (i in 1 until length) {
        val charOfThis = get(i)
        var charOfOriginal = original[i]
        if (charOfThis != charOfOriginal) {
            var j = i + 1
            while (j < length) {
                charOfOriginal = original[j]
                if (charOfThis == charOfOriginal) {
                    break
                }
                j += 1
            }
            if (j == length) {
                var isValid = false
                for (k in 0 until charsLength) {
                    if (charOfThis == chars[k]) {
                        isValid = true
                        break
                    }
                }
                if (!isValid) {
                    return false
                }
            }
            chars[charsLength] = original[i]
            charsLength += 1
            changes += 1
        }
        if (changes > maxOfChanges) {
            return false
        }
    }

    return true
}

fun CharSequence.haveTypo(original: String): Boolean {
    val selfLength = length
    val originalLength = original.length
    val lengthDifference = selfLength - originalLength
    val minLength = if (selfLength == 0 || originalLength == 0 || lengthDifference.absoluteValue > 1) {
        return false
    } else if (length < original.length) {
        length
    } else {
        original.length
    }
    var selfOffset = 0
    var originalOffset = 0
    var changes = 0
    for (i in 0 until minLength) {
        if (get(i + selfOffset) != original[i + originalOffset]) {
            changes += 1
            if (lengthDifference > 0) {
                selfOffset += 1
            } else if (lengthDifference < 0) {
                originalOffset += 1
            }
        }

        if (changes > 1) {
            return false
        }
    }
    return true
}
