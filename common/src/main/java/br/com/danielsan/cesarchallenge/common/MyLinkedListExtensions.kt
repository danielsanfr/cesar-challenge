package br.com.danielsan.cesarchallenge.common

import kotlin.math.absoluteValue

/**
 * Created by daniel on 9/7/18
 */

fun <E : Comparable<E>> MyLinkedList<E>.removeDuplicates() {
    if (size == 0) return
    var referenceNode: MyLinkedList.Node<E>? = head
    while (referenceNode != null) {
        var currentNode: MyLinkedList.Node<E> = referenceNode
        var nextNode: MyLinkedList.Node<E>? = currentNode.next
        while (nextNode != null) {
            if (referenceNode.item == nextNode.item) {
                currentNode.next = nextNode.next
                nextNode.next = null
                nextNode = currentNode.next
            } else {
                currentNode = nextNode
                nextNode = currentNode.next
            }
        }
        referenceNode = referenceNode.next
    }
}

fun <E : Comparable<E>> MyLinkedList<E>.intersectingNode(other: MyLinkedList<E>): MyLinkedList.Node<E>? {
    val size1 = size
    val size2 = other.size
    val sizeDifference = size1 - size2
    var node1: MyLinkedList.Node<E>? = head
    var node2: MyLinkedList.Node<E>? = other.head

    val length = when {
        sizeDifference > 0 -> {
            for (k in 0 until sizeDifference.absoluteValue) {
                node1 = node1?.next
            }
            size1
        }
        sizeDifference < 0 -> {
            for (k in 0 until sizeDifference.absoluteValue) {
                node2 = node2?.next
            }
            size2
        }
        else -> size1
    }

    for (k in sizeDifference.absoluteValue until length) {
        if (node1?.next == null || node2?.next == null) {
            return null
        } else if (node1.next == node2.next) {
            return node1.next
        }
        node1 = node1.next
        node2 = node2.next
    }

    return null
}
