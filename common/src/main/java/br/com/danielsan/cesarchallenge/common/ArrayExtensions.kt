package br.com.danielsan.cesarchallenge.common

/**
 * Created by daniel on 9/7/18
 */

fun Array<Char>.replaceSpaces(trueLength: Int) {
    var i = size - 1
    var j = trueLength - 1
    while (i >= 0 && j >= 0) {
        if (get(j) == ' ') {
            set(i, '2')
            i -= 1
            set(i, '3')
            i -= 1
            set(i, '&')
        } else {
            set(i, get(j))
        }
        i -= 1
        j -= 1
    }
}
