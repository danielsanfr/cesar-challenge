package br.com.danielsan.cesarchallenge.common

/**
 * Created by daniel on 9/7/18
 */
class MyLinkedList<E> {

    val size: Int
        get() {
            var size = 0
            var node: Node<E>? = _head
            while (node != null) {
                node = node.next
                size += 1
            }
            return size
        }
    val head: Node<E>
        get() {
            return _head ?: throw IndexOutOfBoundsException(0.toString())
        }

    private var _head: Node<E>? = null

    override fun toString(): String {
        val stringBuilder = StringBuilder("[")
        var node = _head
        while (node != null) {
            stringBuilder.append(node.item).append(",")
            node = node.next
        }
        if (_head != null) {
            stringBuilder.replace(stringBuilder.length - 1, stringBuilder.length, "]")
        } else {
            stringBuilder.append("]")
        }
        return stringBuilder.toString()
    }

    fun isEmpty() = _head == null

    fun get(index: Int): Node<E> {
        var step = 0
        var node: Node<E>? = _head
        while (node != null && step <= index) {
            node = node.next
            step += 1
        }
        return node ?: throw IndexOutOfBoundsException(index.toString())
    }

    fun add(element: E) = add(Node(element, null))

    fun add(node: Node<E>) {
        if (_head == null) {
            _head = node
        } else {
            var nodeAux = _head
            while (nodeAux != null) {
                if (nodeAux.next == null) {
                    nodeAux.next = node
                    return
                } else {
                    nodeAux = nodeAux.next
                }
            }
        }
    }

    class Node<E>(
        var item: E,
        var next: Node<E>?
    ) {
        fun setNext(next: Node<E>?): Node<E> {
            this.next = next
            return this
        }
    }

}

fun <E> myLinkedListOf(vararg elements: E): MyLinkedList<E> {
    return MyLinkedList<E>().apply {
        for (element in elements) {
            add(element)
        }
    }
}

fun <E> myLinkedListOf(vararg nodes: MyLinkedList.Node<E>): MyLinkedList<E> {
    return MyLinkedList<E>().apply {
        for (node in nodes) {
            add(node)
        }
    }
}
